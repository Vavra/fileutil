usage: fileutil [command] [options]

Commands:

 replace                Replace regex pattern with replace in files (defined by pattern)
 exec                   Execute 'process' in specific direcotry (defined by pattern)

Usage (replace):
 fileutil replace [options] <regexe> <replace>
Options (replace):
 -p, --pattern=<ant_pattern>          ANT pattern for file selection
 -ep, --expattern=<ant_pattern>       Exclude ANT pattern based on pattern
 -b, --base=<dir>,                    Set base directory for patter (default: current directory)
 -k, --keep                           Keep replace file from original. (default: false)
 -b, --backup                         Whether should be create backup file from original (default: false)
 --dry,                               Dry run, no execute replace
 -v, --verbose                        Set base directory for patter (default: current directory)
 -q, --quite                          StdOut will be ignored

Usage (exec):
 fileutil exec [options] <arg1> ... <argN>
Options (exec):

 -p, --pattern=<ant_pattern>          ANT pattern for file selection
 -ep, --expattern=<ant_pattern>       Exclude ANT pattern based on pattern
 -b, --base=<dir>                     Set base directory for patter (default: current directory)
 -t, --threads=<number>               If set then for file is run in thread. (default: 10)
 -v, --verbose                        Verbose output
 -q, --quite                          StdOut will be ignored




