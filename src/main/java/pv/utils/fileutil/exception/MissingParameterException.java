package pv.utils.fileutil.exception;

/**
 *
 */
public class MissingParameterException extends CommandException {

    public MissingParameterException(String message) {
        super(message, 2);
    }
}
