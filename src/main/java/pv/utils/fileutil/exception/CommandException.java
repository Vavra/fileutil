package pv.utils.fileutil.exception;

/**
 *
 */
public class CommandException extends RuntimeException {

    private int status;

    public CommandException(String message, int status) {
        super(message);
        this.status = status;
    }

    public int getStatus() {
        return status;
    }
}
