package pv.utils.fileutil;

import pv.utils.fileutil.utils.IOUtils;
import pv.utils.fileutil.command.Command;
import pv.utils.fileutil.command.impl.ExecCommand;
import pv.utils.fileutil.command.impl.ReplaceCommand;
import pv.utils.fileutil.exception.CommandException;

import java.util.Arrays;
import java.util.List;

public class Application {

    /**
     *
     * @param args
     * @return
     */
    private static List<Command> createCommands(String[] args) {
        return Arrays.asList(

                // replace command
                new ReplaceCommand(args),
                new ExecCommand(args)
        );
    }

    public static void main(String[] args) throws Exception {
        String command;
        List<Command> commands;

        if ( args.length == 0 ) {
            printHelp();
            System.exit(1);
        }

        // init commands


        // fetch command
        command = args[0];
        try {

            System.exit(createCommands(args)
                    .stream()
                    .filter(v->v.support(command, args))
                    .findFirst()
                    .map(v->v.get())
                    .orElseGet(()->{
                        System.out.println(String.format("Unrecognized command [%s]", command));
                        printHelp();
                        return 1;
                    }));

        } catch (CommandException e ) {
            System.err.println(e.getMessage());
            printHelp();

            // exist with stastus
            System.exit(e.getStatus());
        }
    }


    /**
     *
     */
    private static void printHelp() {
        try {
            IOUtils.copy(
                    Application.class.getClassLoader().getResourceAsStream("help.txt"),
                    System.out);
        } catch (Exception e) {}
    }
}
