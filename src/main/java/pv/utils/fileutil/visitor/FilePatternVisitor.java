package pv.utils.fileutil.visitor;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class FilePatternVisitor implements FileVisitor<Path>  {

    /**
     *
     */
    public enum Target {
        FILE,
        DIRECTORY
    }

    private String pattern;
    private Target target;
    private String basePath;

    // Regular Expression patterns
    private Pattern regexPattern;
    private Pattern regexParentPattern;

    /**
     * Result of match
     */
    private List<Path> result = new ArrayList<>();

    public FilePatternVisitor(String pattern, Target target, Path basePath) {
        this.pattern = normalize(pattern);
        this.target = target;
        this.basePath = normalize(basePath.toAbsolutePath().toString()) + "/";

        // calculate
        this.regexPattern = Pattern.compile(toRegex(this.pattern));
        this.regexParentPattern = Pattern.compile(toRegex(toParentPattern(this.pattern)));
    }

    /**
     *
     * @param value
     * @return
     */
    private String normalize(String pattern) {
        // replace "\" -> "/"
        return pattern
                .replaceAll("\\\\", "/");
    }

    /**
     *
     * @param value
     * @return
     */
    private String toRegex(String pattern) {
        return pattern
                .replaceAll("\\.", "\\\\.")
                .replaceAll("\\*\\*/", "([a-zA-Z0-9-_.\\s]+/)+")
                .replaceAll("\\*", "[a-zA-Z0-9-_.\\s]+");
    }

    /**
     *
     * @param value
     * @return
     */
    private String toParentPattern(String pattern) {
        return pattern
                .replaceAll("/[^/]+$", "/");
    }

    /**
     *
     * @return
     */
    public List<Path> getResult() {
        return result;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        if ( Target.DIRECTORY.equals(target)) {

            // add to result (if matches)
            if ( regexPattern.matcher(normalize(dir.toString()).replace(basePath, "")).matches()) {
                result.add(dir);
            }
        }

        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        if ( Target.FILE.equals(target)) {

            // add to result (if matches)
            if ( regexPattern.matcher(normalize(file.toString()).replace(basePath, "")).matches()) {
                result.add(file);
            }
        }

        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        return FileVisitResult.CONTINUE;
    }
}
