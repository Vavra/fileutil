package pv.utils.fileutil.command.impl;

import pv.utils.fileutil.utils.ExceptionUtils;
import pv.utils.fileutil.utils.IOUtils;
import pv.utils.fileutil.utils.StringUtils;
import pv.utils.fileutil.visitor.FilePatternVisitor;
import pv.utils.fileutil.exception.MissingParameterException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Usage:
 *
 * utils exec [options] <command> <argument> .... <argument>
 *
 * Options:
 * -p, --pattern=<ant_pattern>          ANT pattern for file selection
 * -ep, --epattern=<ant_pattern>        Exclude ANT pattern based on pattern
 * -b, --base=<dir>                     Set base directory for patter (default: current directory)
 * -t, --threads=<number>               If set then for file is run in thread. (default: 10)
 * -v, --verbose                        Verbose output
 * -q, --quite                          StdOut will be ignored
 */
public class ExecCommand extends AbtractCommand {

    public ExecCommand(String[] args) {
        super(args);
    }

    @Override
    public boolean support(String command, String[] args) {
        return "exec".equals(command);
    }

    @Override
    public Map<String, List<String>> getSupportedParameters() {
        return Map.of(
                "pattern", Arrays.asList("--pattern", "-p"),
                "epattern", Arrays.asList("--epattern", "-ep"),
                "base", Arrays.asList("--base", "-b"),
                "verbose", Arrays.asList("--verbose", "-v"),
                "threads", Arrays.asList("--threads", "-t"),
                "quite", Arrays.asList("--quite", "-q")
        );
    }

    /**
     *
     * @param parameters
     * @return
     */
    protected Collection<Path> fetchPaths(Path base) throws Exception {

        // patterns
        Set<Path> result = parameters.getParameters("pattern")
                .stream()
                .map(v->{
                    FilePatternVisitor visitor = new FilePatternVisitor(
                            v.getValue(),
                            FilePatternVisitor.Target.DIRECTORY,
                            base);

                    try {
                        // fetch all
                        Files.walkFileTree(base, visitor);
                        return visitor.getResult();
                    } catch ( Exception e ) {
                        throw new RuntimeException(e);
                    }
                })
                .flatMap(v->v.stream())
                .collect(Collectors.toSet());

        // exluded
        if ( !parameters.getParameters("epattern").isEmpty()) {
            parameters.getParameters("epattern")
                    .stream()
                    .map(v->{
                        FilePatternVisitor visitor = new FilePatternVisitor(
                                v.getValue(),
                                FilePatternVisitor.Target.DIRECTORY,
                                base);

                        try {
                            // fetch all
                            Files.walkFileTree(base, visitor);
                            return visitor.getResult();
                        } catch ( Exception e ) {
                            throw new RuntimeException(e);
                        }
                    })
                    .flatMap(v->v.stream())
                    .forEach(v->result.remove(v));
        }

        return result;
    }

    /**
     *
     * @param content
     * @return
     */
    protected String replace(String content) {
        Pattern pattern = Pattern.compile(parameters.getArgumentString(0, ""));
        String replace = parameters.getArgumentString(1, "");
        Matcher matcher = pattern.matcher(content);

        // replace all
        return matcher.replaceAll(replace);
    }

    /**
     *
     * @param parameters
     * @param path
     */
    protected void exec(Path path) throws Exception {
    }

    @Override
    protected void runInternal() throws Exception {
        Executor executor;
        Path base;
        List<String> commands;
        List<ExecuteCommand> executes = new ArrayList<>();

        // check parameters
        parameters.getParameter("pattern").orElseThrow(()->new MissingParameterException("Missing option '--pattern'"));
        parameters.getArgument(0).orElseThrow(()->new MissingParameterException("Missing <command> "));

        // assembly command
        commands = parameters.getArguments().stream().map(v->v.getValue()).collect(Collectors.toList());

        // fetch paths
        base = Paths.get(parameters.getParameterString("base", "")).toAbsolutePath();
        Collection<Path> paths = fetchPaths(base);
        log(LogLevel.DEBUG, sb->{
            sb.append("Recognized folders: \n");
            paths.stream().map(v->v.toAbsolutePath().toString()).forEach(v->sb.append(v).append("\n"));
            sb.append("\n");
        });

        // create executors
        if ( parameters.getParameter("threads").isPresent() ) {
            int coreSize = parameters.getParameterInteger("threads", 10);

            // create executor
            executor = new ThreadPoolExecutor(coreSize, coreSize, 100, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>());
        } else {

            // simple executor
            executor = new Executor() {
                @Override
                public void execute(Runnable command) {
                    command.run();
                }
            };
        }

        // run replace
        for (Path path : paths) {
            ExecuteCommand executeCommand = new ExecuteCommand(path, base, commands, Collections.emptyMap());

            // add and execute
            executes.add(executeCommand);
            executor.execute(executeCommand);
        }

        // wait
        if ( executor instanceof ExecutorService ) {

            // shutdown and wait for finnish
            ((ExecutorService) executor).shutdown();
            ((ExecutorService) executor).awaitTermination(10, TimeUnit.MINUTES);
        }
    }

    /**
     *
     */
    public class ExecuteCommand implements Runnable {

        /**
         * Base directory
         */
        private Path path;
        private Path base;
        private List<String> commands;
        private Map<String, String> environments;

        // output
        private String stdOut;
        private String errOut;
        private int status;

        public ExecuteCommand(Path path, Path base, List<String> commands, Map<String, String> environments) {
            this.path = path;
            this.base = base;
            this.commands = commands;
            this.environments = environments;
        }

        /**
         *
         * @return
         */
        protected Map<String, String> createPlaceholders() {
            return Map.of(
                    "VAR_ABSOLUTE_PATH", path.toAbsolutePath().toString(),
                    "VAR_PATH", path.getFileName().toString(),
                    "VAR_PARENT_PATH", path.toAbsolutePath().getParent().toString(),
                    "VAR_BASE_PATH", base.toAbsolutePath().toString()
                    );
        }

        @Override
        public void run() {
            Map<String, String> placeholders = createPlaceholders();

            ExecCommand.this.log(LogLevel.DEBUG, sb->{
                sb.append(String.format("[%s]-> cmd=\"%s\"\n", path, commands));
            });

            try {
                ProcessBuilder builder = new ProcessBuilder(commands.stream().map(v-> StringUtils.replacePlaceholders(v, placeholders)).collect(Collectors.toList()));
                builder.environment().putAll(environments);

                // start process
                Process process = builder.directory(path.toFile())
                        .start();

                // catch
                stdOut = IOUtils.toString(process.getInputStream());
                errOut = IOUtils.toString(process.getErrorStream());
                status = process.exitValue();

                // status
                if ( status == 0 ) {
                    log(LogLevel.SUCCESS, sb->{
                        sb.append(String.format("[%s]-> cmd=\"%s\", status=[%s]\n", path, commands, status));
                    });
                } else {
                    log(LogLevel.ERROR, sb->{
                        sb.append(String.format("[%s]-> cmd=\"%s\", status=[%s]\n", path, commands, status));
                    });
                }

                // out print
                if ( !StringUtils.isEmpty(stdOut)
                        && Boolean.FALSE.equals(parameters.getParameterBoolean("quite", Boolean.FALSE))) {
                    log(LogLevel.INFO, sb->sb.append(String.format("[%s]-> %s\n", path, stdOut)));
                    System.out.println();
                }

                // err ostoud
                if ( !StringUtils.isEmpty(errOut)) {

                    // sometimes command return status different than 0
                    if ( status == 0 && Boolean.FALSE.equals(parameters.getParameterBoolean("quite", Boolean.FALSE))) {
                        log(LogLevel.INFO, sb->sb.append(String.format("[%s]-> %s\n", path, errOut)));
                    } else if ( status != 0) {
                        log(LogLevel.ERROR, sb->sb.append(String.format("[%s]-> %s\n", path, errOut)));
                    }
                }

            } catch ( Exception e ) {
                log(LogLevel.ERROR, sb->{
                    sb.append(String.format("[%s]-> cmd=\"%s\"; FAILED\n", path, commands));
                    sb.append(ExceptionUtils.stacktrace(e));
                });
            }
        }
    }
}
