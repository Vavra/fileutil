package pv.utils.fileutil.command.impl;

import pv.utils.fileutil.utils.Expressions;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Parameters {

    /**
     * List of parameter
     */
    List<Parameter> parameters = new ArrayList<>();


    public Parameters() {}

    public List<Parameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<Parameter> parameters) {
        this.parameters = parameters;
    }

    /**
     *
     * @param key
     * @return
     */
    public List<Parameter> getParameters(String key) {
        return parameters.stream().filter(v->key.equals(v.getKey())).collect(Collectors.toList());
    }

    /**
     *
     * @param key
     * @return
     */
    public Optional<Parameter> getParameter(String key) {
        return parameters.stream().filter(v->key.equals(v.getKey())).findFirst();
    }

    /**
     *
     * @param key
     * @return
     */
    public String getParameterString(String key, String defaultValue) {
        return getParameter(key).map(Parameter::getValue).orElse(defaultValue);
    }

    /**
     *
     * @param key
     * @return
     */
    public Boolean getParameterBoolean(String key, Boolean defaultValue) {
        return getParameter(key).map(v->Boolean.valueOf(v.getValue())).orElse(defaultValue);
    }

    /**
     *
     * @param key
     * @return
     */
    public Integer getParameterInteger(String key, Integer defaultValue) {
        return getParameter(key).map(v->Integer.valueOf(v.getValue())).orElse(defaultValue);
    }

    /**
     *
     * @param key
     * @return
     */
    public Long getParameterLong(String key, Long defaultValue) {
        return getParameter(key).map(v->Long.valueOf(v.getValue())).orElse(defaultValue);
    }



    /**
     *
     * @param key
     * @return
     */
    public List<Parameter> getArguments() {
        return parameters.stream().filter(v->v.getType().equals(Parameter.Type.ARG)).collect(Collectors.toList());
    }

    /**
     *
     * @param key
     * @return
     */
    public Optional<Parameter> getArgument(int index) {
        return Expressions.tryGet(()->Optional.ofNullable(parameters.stream().filter(v->v.getType().equals(Parameter.Type.ARG)).collect(Collectors.toList()).get(index)), Optional.empty());
    }

    /**
     *
     * @param key
     * @return
     */
    public String getArgumentString(int index, String defaultValue ) {
        return getArgument(index).map(v->v.getValue()).orElse(defaultValue);
    }

    /**
     *
     * @param key
     * @return
     */
    public Integer getArgumentInteger(int index, Integer defaultValue ) {
        return getArgument(index).map(v->Integer.valueOf(v.getValue())).orElse(defaultValue);
    }
}
