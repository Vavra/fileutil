package pv.utils.fileutil.command.impl;

import pv.utils.fileutil.utils.IOUtils;
import pv.utils.fileutil.visitor.FilePatternVisitor;
import pv.utils.fileutil.exception.MissingParameterException;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Usage:
 *
 * utils replace [options] <regex> <replace>
 *
 * Options:
 * -p, --pattern=<ant_pattern>          ANT pattern for file selection
 * -ep, --expattern=<ant_pattern>       Exclude ANT pattern based on pattern
 * -b, --base=<dir>,                    Set base directory for patter (default: current directory)
 * -k, --keep                           Keep replace file from original. (default: false)
 * -b, --backup                         Whether should be create backup file from original (default: false)
 * --dry,                               Dry run, no execute replace
 * -v, --verbose                        Set base directory for patter (default: current directory)
 *
 */
public class ReplaceCommand extends AbtractCommand {

    public ReplaceCommand(String[] args) {
        super(args);
    }

    @Override
    public boolean support(String command, String[] args) {
        return "replace".equals(command);
    }

    @Override
    public Map<String, List<String>> getSupportedParameters() {
        return Map.of(
                "pattern", Arrays.asList("--pattern", "-p"),
                "epattern", Arrays.asList("--epattern", "-ep"),
                "base", Arrays.asList("--base", "-b"),
                "keep", Arrays.asList("--keep"),
                "backup", Arrays.asList("--backup", "-b"),
                "dry", Arrays.asList("--dry"),
                "verbose", Arrays.asList("--verbose", "-v")
        );
    }

    /**
     *
     * @param parameters
     * @return
     */
    protected Collection<Path> fetchPaths(Path base) throws Exception {
        Set<Path> result = parameters.getParameters("pattern")
                .stream()
                .map(v->{
                    FilePatternVisitor visitor = new FilePatternVisitor(
                            v.getValue(),
                            FilePatternVisitor.Target.FILE,
                            base);

                    try {
                        // fetch all
                        Files.walkFileTree(base, visitor);
                        return visitor.getResult();
                    } catch ( Exception e ) {
                        throw new RuntimeException(e);
                    }
                })
                .flatMap(v->v.stream())
                .collect(Collectors.toSet());

        // exluded
        if ( !parameters.getParameters("epattern").isEmpty()) {
            parameters.getParameters("epattern")
                    .stream()
                    .map(v->{
                        FilePatternVisitor visitor = new FilePatternVisitor(
                                v.getValue(),
                                FilePatternVisitor.Target.FILE,
                                base);

                        try {
                            // fetch all
                            Files.walkFileTree(base, visitor);
                            return visitor.getResult();
                        } catch ( Exception e ) {
                            throw new RuntimeException(e);
                        }
                    })
                    .flatMap(v->v.stream())
                    .forEach(v->result.remove(v));
        }

        return result;
    }

    /**
     *
     * @param content
     * @return
     */
    protected String replace(String content) {
        Pattern pattern = Pattern.compile(parameters.getArgumentString(0, ""));
        String replace = parameters.getArgumentString(1, "");
        Matcher matcher = pattern.matcher(content);

        // replace all
        return matcher.replaceAll(replace);
    }

    /**
     *
     * @param parameters
     * @param path
     */
    protected void replace(Path path) throws Exception {
        Path sourcePath = path;
        Path sourcePathBackup = Path.of(String.format("%s.backup",sourcePath));
        Path destPath = Path.of(String.format("%s.replace", sourcePath));
        String content;
        String result;
        boolean replaced;

        // log
        log(LogLevel.DEBUG, sb->{
            sb.append(String.format("Processing file=[%s]\n", sourcePath.toString()));
        });

        // load to String
        try ( FileInputStream in = new FileInputStream(sourcePath.toFile())) {
            content = IOUtils.toString(in, Charset.defaultCharset());
        }

        // replace in content
        result = replace(content);

        // create new file with
        try (FileOutputStream out = new FileOutputStream(destPath.toFile(), false)) {
            IOUtils.copy(new ByteArrayInputStream(result.getBytes(Charset.defaultCharset())), out);
        }

        // backup
        if ( Boolean.TRUE.equals(parameters.getParameterBoolean("backup", Boolean.FALSE))) {
            // log
            log(LogLevel.DEBUG, sb->{
                sb.append(String.format("Creating backup file=[%s]\n", sourcePathBackup));
            });

            Files.copy(sourcePath, sourcePathBackup, StandardCopyOption.REPLACE_EXISTING);
        }

        // replace file
        if ( Boolean.FALSE.equals(parameters.getParameterBoolean("dry", Boolean.FALSE))) {
            // log
            log(LogLevel.DEBUG, sb->{
                sb.append(String.format("Moving source=[%s] to dest=[%s]\n", destPath, sourcePath));
            });
            Files.copy(destPath, sourcePath, StandardCopyOption.REPLACE_EXISTING);
        }

        // keep
        if ( Boolean.FALSE.equals(parameters.getParameterBoolean("keep", Boolean.FALSE))) {
            // log
            log(LogLevel.DEBUG, sb->{
                sb.append(String.format("Delete replace file=[%s]\n", destPath));
            });
            Files.delete(destPath);
        }

        // success
        if ( !result.equals(content)) {
            log(LogLevel.SUCCESS, sb->{
                sb.append(String.format("[%s]-> replaced\n", path.toAbsolutePath()));
            });
        } else {
            log(LogLevel.WARNING, sb->{
                sb.append(String.format("[%s]-> not replaced\n", path.toAbsolutePath()));
            });
        }
    }

    @Override
    protected void runInternal() throws Exception {
        Path base;

        // check parameters
        parameters.getParameter("pattern").orElseThrow(()->new MissingParameterException("Missing option '--pattern'"));
        parameters.getArgument(0).orElseThrow(()->new MissingParameterException("Missing argument <regex>"));
        parameters.getArgument(1).orElseThrow(()->new MissingParameterException("Missing argument <replace>"));

        // fetch paths
        base = Paths.get(parameters.getParameterString("base", ""));
        Collection<Path> paths = fetchPaths(base);
        log(LogLevel.DEBUG, sb->{
            sb.append("Recognized files:\n");
            paths.stream().map(v->v.toAbsolutePath().toString()).forEach(v->sb.append(v).append("\n"));
        });

        // run replace
        for (Path path : paths) {
            replace(path);
        }
    }
}
