package pv.utils.fileutil.command.impl;

import pv.utils.fileutil.utils.ConsoleUtils;
import pv.utils.fileutil.utils.RegexUtils;
import pv.utils.fileutil.command.Command;
import pv.utils.fileutil.exception.CommandException;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public abstract class AbtractCommand implements Command {
    /**
     * Parameters
     */
    protected Parameters parameters;

    /**
     * All argsuments
     */
    protected String[] args;

    public AbtractCommand(String[] args) {
        this.args = args;
    }

    /**
     * Wheteher support or not
     * @param command
     * @param args
     * @return
     */
    @Override
    public abstract boolean support(String command, String[] args);

    /**
     *
     * @return
     */
    public abstract Map<String, List<String>> getSupportedParameters();

    /**
     *
     * @param args
     * @throws Exception
     */
    protected abstract void runInternal() throws Exception;

    /**
     *
     * @param level
     * @param parameters
     * @param supplier
     */
    protected void log(LogLevel level, Consumer<StringBuilder> consumer) {
        PrintStream pw;
        String color;
        StringBuilder builder = new StringBuilder();

        // block debug (if it's not set)
        if ( LogLevel.DEBUG == level
                && Boolean.FALSE.equals(parameters.getParameterBoolean("verbose", Boolean.FALSE))) {
            return;
        }

        // consume
        consumer.accept(builder);

        // log
        switch ( level ) {
            case ERROR:
                ConsoleUtils.error(builder.toString(), System.err);
                break;
            case DEBUG:
                ConsoleUtils.debug(builder.toString(), System.out);
                break;
            case INFO:
                ConsoleUtils.normal(builder.toString(), System.out);
                break;
            case SUCCESS:
                ConsoleUtils.success(builder.toString(), System.out);
                break;
            case WARNING:
                ConsoleUtils.warning(builder.toString(), System.out);
                break;
            default:
                ConsoleUtils.normal(builder.toString(), System.out);
        }
    }

    /**
     *
     * @param args
     * @return
     */
    protected Parameters parse(String[] args) {
        Parameters parameters = new Parameters();
        Map<Integer, String> groups;

        // create reverse maps for supported parameters
        Map<String, String> supportedParameters = getSupportedParameters()
                .entrySet()
                .stream()
                .reduce(new HashMap<>(),
                        (r,i)->{
                    i.getValue().forEach(v->r.put(v, i.getKey()));
                    return r;
                    }, (r1,r2)->r1);

        for ( int index = 1; index < args.length; index++  ) {
            String arg = args[index];

            // parse
            groups = RegexUtils.groups(arg,"([a-zA-Z0-9-_.]+)(=\"?(.+)\"?)?");
            if ( groups != null ) {
                String parameter = groups.get(1);
                String value = groups.get(3);
                String key;

                // check againsts supported parameter
                key = supportedParameters.get(parameter);
                if ( key != null ) {
                    parameters.getParameters().add(new Parameter(key, value != null ? value : "true"));
                    continue;
                }
            }

            parameters.getParameters().add(new Parameter(arg));
        }

        return parameters;
    }

    @Override
    public Integer get() {
        try {

            // parse parameters
            this.parameters = parse(args);

            // run internat
            runInternal();

        } catch ( CommandException e ) {
          throw e;
        } catch (Exception e ) {
            return 1;
        }

        return 0;
    }


    /**
     *
     */
    enum  LogLevel {
        INFO,
        SUCCESS,
        WARNING,
        DEBUG,
        ERROR
    }
}
