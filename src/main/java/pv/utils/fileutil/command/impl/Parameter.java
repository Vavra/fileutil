package pv.utils.fileutil.command.impl;

/**
 *
 * @param key
 * @param value
 */
public class Parameter {

    enum Type {

        KEY,
        ARG
    }

    private String key;
    private String value;
    private Type type;

    public Parameter(String value) {
        this.value = value;
        type = Type.ARG;

    }

    public Parameter(String key, String value) {
        this.key = key;
        this.value = value;
        type = Type.KEY;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public Type getType() {
        return type;
    }
};