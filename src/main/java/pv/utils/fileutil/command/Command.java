package pv.utils.fileutil.command;

import java.util.function.Supplier;

/**
 *
 */
public interface Command extends Supplier<Integer> {

    /**
     *
     * @param args
     * @return
     * @throws Exception
     */
    boolean support(String command, String[] args);
}
