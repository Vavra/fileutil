package pv.utils.fileutil.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtils {

    /**
     * Find string defined by regexPattern inside input.
     * @param input String on which matching should be done
     * @param regexPattern Pattern to look for
     * @return Map with group number and result
     */
    public static Map<Integer, String> groups(String input, String pattern) {
        Map<Integer, String> result = new HashMap<>();

        Pattern compiled = Pattern.compile(pattern);
        Matcher matcher = compiled.matcher(input);
        if (matcher.find()) {
            for(int i = 1; i <= matcher.groupCount(); i++) {
                result.put(i, matcher.group(i));
            }
            return result;
        } else {
            return null;
        }
    }
}
