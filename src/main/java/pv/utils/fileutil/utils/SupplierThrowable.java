package pv.utils.fileutil.utils;

@FunctionalInterface
public interface SupplierThrowable<T> {

    /**
     * Gets a result.
     *
     * @return a result
     */
    T get() throws Exception;
}
