package pv.utils.fileutil.utils;

import java.util.Map;

/**
 *
 */
public class StringUtils {

    /**
     *
     * @param value
     * @return
     */
    public static boolean isEmpty(String value) {
        return value == null || value.length() == 0;
    }

    /**
     *
     * @param value
     * @param pattern
     * @param replace
     * @return
     */
    public static String replacePlaceholders(String value, Map<String, String> placeholders) {
        for ( Map.Entry<String, String> entry : placeholders.entrySet()) {
            value = value.replace(String.format("{%s}", entry.getKey()), entry.getValue());
        }
        return value;
    }
}
