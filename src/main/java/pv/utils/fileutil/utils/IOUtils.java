package pv.utils.fileutil.utils;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Collection;

public class IOUtils {

    private static final int BUFFER_LINE_DEFAULT            = 4096;
    private static final String CHARSET_DEFAULT            = "utf-8";

    /**
     *
     * @param in
     * @param charset
     * @return
     * @throws Exception
     */
    public static String toString(InputStream in, Charset charset) throws Exception {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        copy(in, out);
        return new String(out.toByteArray(), charset);
    }

    /**
     * UTF-8
     * @param in
     * @param charset
     * @return
     * @throws Exception
     */
    public static String toString(InputStream in) throws Exception {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        copy(in, out);
        return new String(out.toByteArray(), Charset.forName(CHARSET_DEFAULT));
    }

    /**
     *
     * @param in
     * @param out
     */
    public static void copy(InputStream in, OutputStream out) throws Exception {
        copy(in, out, BUFFER_LINE_DEFAULT);
    }

    /**
     *
     * @param in
     * @param out
     */
    public static void copy(InputStream in, OutputStream out, int bufferSize) throws Exception {
        byte[] buffer = new byte[bufferSize];
        int size;
        while ( (size = in.read(buffer)) > 0 ) {
            out.write(buffer, 0, size);
            out.flush();
        }
    }

    /**
     *
     * @param lines
     * @param outputStream
     * @param newLine
     */
    public static void copy(Collection<String> datas, OutputStream out, String separator) throws IOException {
        int bufferSize = 0;

        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out))) {
            for ( String data : datas ) {
                if ( data != null ) {
                    bw.write(data);
                    bufferSize += data.length();
                    if ( separator != null ) {
                        bw.write(separator);
                        bufferSize += separator.length();
                    }

                    // flush regularaly
                    if ( bufferSize > BUFFER_LINE_DEFAULT ) {
                        bw.flush();
                    }
                }
            }

            // flush
            bw.flush();
        }
    }
}
