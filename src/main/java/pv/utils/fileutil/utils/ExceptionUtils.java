package pv.utils.fileutil.utils;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class ExceptionUtils {


    /**
     *
     * @param throwable
     * @return
     */
    public static String stacktrace(Throwable throwable) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        throwable.printStackTrace(new PrintStream(out));
        return new String(out.toByteArray());
    }
}
