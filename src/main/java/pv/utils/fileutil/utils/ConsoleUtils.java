package pv.utils.fileutil.utils;

import java.io.PrintStream;

public class ConsoleUtils {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    /**
     *
     * @param value
     * @param out
     */
    public static void error(String value, PrintStream out) {
        out.print(String.format("%s%s%s", ANSI_RED, value, ANSI_RESET));
    }

    /**
     *
     * @param value
     * @param out
     */
    public static void debug(String value, PrintStream out) {
        out.print(String.format("%s", value));
    }

    /**
     *
     * @param value
     * @param out
     */
    public static void warning(String value, PrintStream out) {
        out.print(String.format("%s%s%s", ANSI_YELLOW, value, ANSI_RESET));
    }

    /**
     *
     * @param value
     * @param out
     */
    public static void success(String value, PrintStream out) {
        out.print(String.format("%s%s%s", ANSI_GREEN, value, ANSI_RESET));
    }

    /**
     *
     * @param value
     * @param out
     */
    public static void normal(String value, PrintStream out) {
        out.print(String.format("%s", value));
    }
}
