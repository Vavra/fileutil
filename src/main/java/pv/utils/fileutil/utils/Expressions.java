package pv.utils.fileutil.utils;

import java.util.function.Supplier;

public class Expressions {

    /**
     * Try fetch objects
     * @param value
     * @param supplier
     * @param <T>
     * @return
     */
    public static <T> T tryGetException(SupplierThrowable<T> supplier, T defaultValue) {
        try {
            return supplier.get();
        } catch ( Exception e ) {
            return defaultValue;
        }
    }

    /**
     * Try fetch objects
     * @param value
     * @param supplier
     * @param <T>
     * @return
     */
    public static <T> T tryGetException(SupplierThrowable<T> supplier) {
        return tryGetException(supplier, null);
    }


    /**
     * Try fetch objects
     * @param value
     * @param supplier
     * @param <T>
     * @return
     */
    public static <T> T tryGet(Supplier<T> supplier) {
        return tryGet(supplier, null);
    }

    /**
     * @param supplier
     * @param defaultValue
     * @param <T>
     * @return
     */
    public static <T> T tryGet(Supplier<T> supplier, T defaultValue) {
        return tryGetException(()->supplier.get(), defaultValue);
    }

    /**
     *
     * @param values
     * @return
     * @param <T>
     */
    public static <T> T firstNotNull(T... values) {
        for (T value : values) {
            if (value != null) {
                return value;
            }
        }
        return null;
    }

    /**
     * Try fetch objects
     * @param value
     * @param supplier
     * @param <T>
     * @return
     */
    public static <T> T value(T value, Supplier<T> defaultValue) {
        return value != null ? value : defaultValue.get();
    }

    /**
     * Try fetch objects
     * @param value
     * @param supplier
     * @param <T>
     * @return
     */
    public static <T> T value(T value, T defaultValue) {
        return value(value, ()->defaultValue);
    }
}
